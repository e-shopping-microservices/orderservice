package order_Service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import order_Service.dto.OrderReqDTO;
import order_Service.dto.OrderResDTO;
import order_Service.service.OrderQueryService;
import order_Service.service.OrderService;
import order_Service.utils.EntityDTO;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/order")
public class OrderController {
	
	@Autowired
	private OrderService service;
	
	@Autowired
	private OrderQueryService queryService;
	
	@PostMapping
	public Mono<ResponseEntity<OrderResDTO>> orderPrd(@RequestBody Mono<OrderReqDTO> dto) {
		return service.doOrder(dto)
				.onErrorReturn(WebClientResponseException.class, ResponseEntity.badRequest().build())
				.onErrorReturn(WebClientRequestException.class, ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build());
	}
	
	@GetMapping("/user/{id}")
	public Flux<OrderResDTO> getOrders(@PathVariable int id) {
		return Flux.fromStream(() -> this.queryService.listOrders(id).stream())
				.map(EntityDTO::getOrderResDTO);
	}

}
