package order_Service.utils;

import order_Service.client.ClientException;
import order_Service.dto.ClientExceptionDTO;
import order_Service.dto.OrderResDTO;
import order_Service.dto.OrderStatus;
import order_Service.dto.RequestContext;
import order_Service.dto.TransactionRequestDTO;
import order_Service.dto.TransactionStatus;
import order_Service.entity.PurchaseOrder;

public class EntityDTO {
	
	public static ClientExceptionDTO getClientExceptionDTO(ClientException exp) {
		ClientExceptionDTO dto = new ClientExceptionDTO();
		dto.setMessage(exp.getMessage());
		dto.setName(exp.getSerName());
		dto.setStatus(exp.getStatus());
		return dto;
	}
	
	public static void setTransReqDTO(RequestContext con) {
		TransactionRequestDTO dto = new TransactionRequestDTO();
		dto.setAmount(con.getProductDTO().getPrice());
		dto.setUserId(con.getOrderReqDTO().getUserId());
		con.setTransReqDTO(dto);
	}
	
	public static PurchaseOrder getPurchaseOrder(RequestContext rc) {
		PurchaseOrder po = new PurchaseOrder();
		po.setAmount(rc.getTransResDTO().getAmount());
		po.setUserId(rc.getOrderReqDTO().getUserId());
		po.setProductId(rc.getProductDTO().getId());
		
		TransactionStatus status = rc.getTransResDTO().getStatus();
		OrderStatus ostatus = TransactionStatus.APPROVED.equals(status) ? OrderStatus.COMPLETED : OrderStatus.FAILED; 
		po.setStatus(ostatus);
		return po;
	}
	
	public static OrderResDTO getOrderResDTO(PurchaseOrder po) {
		OrderResDTO dto = new OrderResDTO();
		dto.setAmount(po.getAmount());
		dto.setProductId(po.getProductId());
		dto.setStaus(po.getStatus());
		dto.setUserId(po.getUserId());
		return dto;
	}

}
