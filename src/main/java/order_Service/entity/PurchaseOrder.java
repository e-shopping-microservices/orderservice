package order_Service.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.ToString;
import order_Service.dto.OrderStatus;

@Entity
@Data
@ToString
public class PurchaseOrder {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;
	@Column(name = "user_id")
	private Integer userId;
	@Column(name = "prd_id")
	private String productId;
	private Integer amount;
	private OrderStatus status;
}
