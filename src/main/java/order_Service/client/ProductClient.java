package order_Service.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import order_Service.dto.ProductDTO;
import reactor.core.publisher.Mono;

@Service
public class ProductClient {
	
	private WebClient webClient;
	
	public ProductClient(@Value("${product.service.url}") String url) {
		this.webClient = WebClient
		.builder()
		.baseUrl(url)
		.build();
	}
	
	public Mono<ProductDTO> getPrdById(String id) {
		return this.webClient
		.get()
		.uri("/{id}", id)
		.retrieve()
		.bodyToMono(ProductDTO.class);
	}
}
