package order_Service.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import order_Service.dto.TransactionRequestDTO;
import order_Service.dto.TransactionResDTO;
import reactor.core.publisher.Mono;

@Service
public class UserClient {

    private WebClient webClient;
	
	public UserClient(@Value("${user.service.url}") String url) {
		this.webClient = WebClient
		.builder()
		.baseUrl(url)
		.build();
	}
	
	public Mono<TransactionResDTO> doTransaction(TransactionRequestDTO dto) {
		return this.webClient
		.post()
		.uri("transaction")
		.bodyValue(dto)
		.retrieve()
		.bodyToMono(TransactionResDTO.class);
	}
}
