package order_Service.client;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientException extends RuntimeException {
	
	private String serName;
	private String message;
	private int status;

}
