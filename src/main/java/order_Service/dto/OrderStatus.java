package order_Service.dto;

public enum OrderStatus {

	COMPLETED, FAILED
}
