package order_Service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class TransactionResDTO {
	
	
	private Integer userId;
	private Integer amount;
	private TransactionStatus status;

}
