package order_Service.dto;

public enum TransactionStatus {
	
	APPROVED, 
	DECLINED

}
