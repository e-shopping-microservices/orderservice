package order_Service.dto;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class TransactionRequestDTO {

	private Integer userId;
	private Integer amount;

}
