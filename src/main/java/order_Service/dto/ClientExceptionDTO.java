package order_Service.dto;

import lombok.Data;

@Data
public class ClientExceptionDTO {

	private String message;
	private int status;
	private String name;
	
}
