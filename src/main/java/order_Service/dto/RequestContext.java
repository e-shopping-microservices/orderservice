package order_Service.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class RequestContext {

	private OrderReqDTO orderReqDTO;
	private ProductDTO productDTO;
	private TransactionRequestDTO transReqDTO;
	private TransactionResDTO transResDTO;
	
	public RequestContext(OrderReqDTO dto) {
		this.orderReqDTO = dto;
	}
}
