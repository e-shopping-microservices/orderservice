package order_Service.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ProductDTO {
	
	private String id, description;
	private Integer price;

}
