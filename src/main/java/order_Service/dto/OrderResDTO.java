package order_Service.dto;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class OrderResDTO {

	private Integer userId;
	private String productId;
	private Integer amount;
	private OrderStatus staus;
}
