package order_Service.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class OrderReqDTO {
	
	private Integer userId;
	private String productId;

}
