package order_Service.service;

import java.time.Duration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;

import order_Service.client.ClientException;
import order_Service.client.ProductClient;
import order_Service.client.UserClient;
import order_Service.dto.OrderReqDTO;
import order_Service.dto.OrderResDTO;
import order_Service.dto.OrderStatus;
import order_Service.dto.RequestContext;
import order_Service.repository.OrderRepo;
import order_Service.utils.EntityDTO;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;
import reactor.util.retry.Retry;

@Service
public class OrderService {
   
	@Autowired
	private OrderRepo order_repo;
	
	@Autowired
	private ProductClient prd_client;
	
	@Autowired
	private UserClient usr_client;
	
	public Mono<ResponseEntity<OrderResDTO>> doOrder(Mono<OrderReqDTO> dto) {
		return dto.map(RequestContext::new)
		.flatMap(this::getPrd)
		.doOnNext(EntityDTO::setTransReqDTO)
		.flatMap(this::transact)
		.map(EntityDTO::getPurchaseOrder)
		.map(po -> { 
			if(po.getStatus().equals(OrderStatus.COMPLETED))
				return ResponseEntity.ok(EntityDTO.getOrderResDTO(order_repo.save(po)));
		    else return ResponseEntity.badRequest().body(EntityDTO.getOrderResDTO(po));
			}
		)
		.subscribeOn(Schedulers.boundedElastic());
		
	}
	
	
	private Mono<RequestContext> getPrd(RequestContext con) {
		return this.prd_client.getPrdById(con.getOrderReqDTO().getProductId())
		.doOnNext(prdRes -> con.setProductDTO(prdRes))
		.retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)))
		.thenReturn(con);
	}
	
	
	private Mono<RequestContext> transact(RequestContext rc) {
		return this.usr_client.doTransaction(rc.getTransReqDTO())
		.doOnNext(rc::setTransResDTO)
		.thenReturn(rc);
	}
}
