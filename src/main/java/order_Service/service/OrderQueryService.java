package order_Service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import order_Service.entity.PurchaseOrder;
import order_Service.repository.OrderRepo;

@Service
public class OrderQueryService {
	
	@Autowired
	private OrderRepo repo;
	
	public List<PurchaseOrder> listOrders(int userId) {
		return repo.findByUserId(userId);			
	}
}
