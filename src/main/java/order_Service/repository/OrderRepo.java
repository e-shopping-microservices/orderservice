package order_Service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import order_Service.entity.PurchaseOrder;

@Repository
public interface OrderRepo extends JpaRepository<PurchaseOrder, Integer> {
	List<PurchaseOrder> findByUserId(int userId);
}
